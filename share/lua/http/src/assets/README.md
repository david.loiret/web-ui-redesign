# Generate SVG icons

https://github.com/MMF-FE/vue-svgicon
$ vsvg -s /path/to/svg/source -t /path/for/generated/components --ext js --es6

## Replace icon import to svgIcon
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon
to
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon

## Remove index.js

# Note about SVG placeholders
SVG placeholders have fill attr, however vsvg replace fill by _fill attr.
After running vsvg we should manually replace _fill by fill attr.