/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Music': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M8.887 21.38a1.767 1.767 0 11-1.774 1.767v-.007c0-.972.788-1.76 1.76-1.76h.014-.001zm14.226-2.667a1.76 1.76 0 11-1.78 1.788V20.5v-.007c0-.968.785-1.753 1.753-1.753h.028-.001zm1.774-11.286v1.76l-12.44 2.34v-1.76zm1.78-2.094l-1.053.22L11.387 8.22l-.72.087v11.807a3.48 3.48 0 00-1.779-.493h-.001a3.527 3.527 0 103.561 3.526v-9.86l12.44-2.313v6.5a3.464 3.464 0 00-1.772-.493h-.001a3.527 3.527 0 103.554 3.527V5.334z"/>'
  }
})
