/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'sort1': {
    width: 18,
    height: 32,
    viewBox: '0 0 18 32',
    data: '<path pid="0" d="M18.286 19.429c0 .304-.125.589-.339.804l-8 8c-.214.214-.5.339-.804.339s-.589-.125-.804-.339l-8-8A1.137 1.137 0 010 19.429c0-.625.518-1.143 1.143-1.143h16c.625 0 1.143.518 1.143 1.143zm0-6.858c0 .625-.518 1.143-1.143 1.143h-16A1.151 1.151 0 010 12.571c0-.304.125-.589.339-.804l8-8c.214-.214.5-.339.804-.339s.589.125.804.339l8 8c.214.214.339.5.339.804z"/>'
  }
})
