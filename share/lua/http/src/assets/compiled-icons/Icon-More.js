/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-More': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M7.113 14.22h-.007A1.78 1.78 0 108.886 16a1.78 1.78 0 00-1.773-1.78zM16 14.22A1.78 1.78 0 1017.78 16 1.787 1.787 0 0016 14.22zM24.887 14.22a1.78 1.78 0 101.78 1.78 1.78 1.78 0 00-1.78-1.78z"/>'
  }
})
