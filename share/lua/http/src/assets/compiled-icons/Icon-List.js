/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-List': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M5.333 26.667h2.113v-2.133H5.333zm6.334 0h15v-2.133h-15zm-6.334-6.4h2.113v-2.133H5.333zm6.334 0h15v-2.133h-15zm-6.334-6.4h2.113v-2.133H5.333zm6.334 0h15v-2.133h-15zm-6.334-6.4h2.113V5.334H5.333zm6.334 0h15V5.334h-15z"/>'
  }
})
