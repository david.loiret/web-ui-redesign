/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Clear-Queue': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M26.667 5.333v2.133H5.334V5.333h21.333zm0 6.4v2.133H5.334v-2.133h21.333zm-12.6 6.4v2.133H5.334v-2.133h8.733zm0 6.4v2.133H5.334v-2.133h8.733zm11.4-6.066l1.2 1.2-2.933 2.933 2.867 2.867-1.2 1.2-2.867-2.867-2.933 2.867-1.2-1.2 2.933-2.933-2.867-2.867 1.2-1.2 2.867 2.867 2.933-2.867z"/>'
  }
})
