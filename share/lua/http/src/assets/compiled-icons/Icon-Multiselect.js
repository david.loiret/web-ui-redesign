/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Multiselect': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M19.8 13.733l-7.6 7.6-2.333-2.267-.733-.733-.8.8 1.533 1.533 1.4 1.333.933.933.933-.933 7.467-7.4-.8-.867zm6.867-8.4v18.2H24.4V22h.733V6.867H9.866v.867H8.333v-2.4h18.333zm-16.8 3.134H23.6v18.2H5.333v-18.2h4.533z"/>'
  }
})
