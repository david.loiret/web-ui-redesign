/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'filter_list_alt': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M5.688 7.563q-.375-.438-.375-.875 0-.563.406-.969t.969-.406h18.625q.563 0 .969.406t.406.969q.063.375-.313.813l-7.688 9.813v8q0 .563-.375.969t-.938.406h-2.688q-.563 0-.969-.406t-.406-.969v-8q-7.5-9.563-7.625-9.75z"/>'
  }
})
