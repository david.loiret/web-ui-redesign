/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Check': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M26.667 9.6l-14 14-7.333-7.333 1.2-1.2 6.133 6.133 12.8-12.8 1.2 1.2z"/>'
  }
})
