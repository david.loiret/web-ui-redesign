/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Pause': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M9.667 5.333H11.8v21.333H9.667V5.333zm10.533 0v21.333h2.133V5.333H20.2z"/>'
  }
})
