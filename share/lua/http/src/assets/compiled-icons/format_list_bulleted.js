/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'format_list_bulleted': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M9.313 6.688h18.688v2.625H9.313V6.688zm0 10.625v-2.625h18.688v2.625H9.313zm0 8v-2.625h18.688v2.625H9.313zm-4-3.313q.813 0 1.406.594T7.313 24t-.594 1.406T5.313 26t-1.406-.594T3.313 24t.594-1.406T5.313 22zm0-16q.813 0 1.406.563t.594 1.438-.594 1.438-1.406.563-1.406-.563-.594-1.438.594-1.438T5.313 6zm0 8q.813 0 1.406.563t.594 1.438-.594 1.438-1.406.563-1.406-.563-.594-1.438.594-1.438T5.313 14z"/>'
  }
})
