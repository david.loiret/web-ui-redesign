/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Favorite': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M16 9.933l1.533 4.933h4.933l-4 3.067 1.533 4.933-4-3.067-4 3.067 1.533-4.933-4-3.067h4.933l1.533-4.933zm0-4.6l-2.533 8.133H5.334l6.6 5.067-2.533 8.133 6.6-5 6.6 5-2.533-8.133 6.6-5.067h-8.133l-2.533-8.133z"/>'
  }
})
