/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'local_movies': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M24 12V9.312h-2.688V12H24zm0 5.313v-2.625h-2.688v2.625H24zm0 5.375V20h-2.688v2.688H24zM10.688 12V9.312H8V12h2.688zm0 5.313v-2.625H8v2.625h2.688zm0 5.375V20H8v2.688h2.688zM24 4h2.688v24H24v-2.688h-2.688V28H10.687v-2.688H7.999V28H5.311V4h2.688v2.688h2.688V4h10.625v2.688H24V4z"/>'
  }
})
