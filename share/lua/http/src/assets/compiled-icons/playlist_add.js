/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'playlist_add': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M2.688 21.313v-2.625h10.625v2.625H2.688zM24 18.688h5.313v2.625H24v5.375h-2.688v-5.375h-5.313v-2.625h5.313v-5.375H24v5.375zM18.688 8v2.688h-16V8h16zm0 5.313v2.688h-16v-2.688h16z"/>'
  }
})
