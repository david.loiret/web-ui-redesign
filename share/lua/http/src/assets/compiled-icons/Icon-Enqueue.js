/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Enqueue': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M5.333 11.733h21.333v2.133H5.333v-2.133zM5.333 5.333h21.333v2.133H5.333V5.333zM5.333 24.533h8.733v2.133H5.333v-2.133zM22.6 20.867v-4.133h-1.667v4.133h-4.067v1.667h4.067v4.133H22.6v-4.133h4.067v-1.667zM5.333 18.133h8.733v2.133H5.333v-2.133z"/>'
  }
})
