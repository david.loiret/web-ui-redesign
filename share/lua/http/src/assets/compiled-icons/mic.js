/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'mic': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M15 22a5 5 0 005-5V5a5 5 0 00-10 0v12a5 5 0 005 5zm7-8v3a7 7 0 11-14 0v-3H6v3a9 9 0 008 8.944V30h-4v2h10v-2h-4v-4.056A9 9 0 0024 17v-3h-2z"/>'
  }
})
