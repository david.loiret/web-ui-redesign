/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'movie': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M24 5.313h5.313v18.688q0 1.063-.781 1.875t-1.844.813H5.313q-1.063 0-1.844-.813t-.781-1.875v-16q0-1.063.781-1.875t1.844-.813h1.375l2.625 5.375h4l-2.625-5.375h2.625l2.688 5.375h4l-2.688-5.375h2.688l2.688 5.375h4z"/>'
  }
})
