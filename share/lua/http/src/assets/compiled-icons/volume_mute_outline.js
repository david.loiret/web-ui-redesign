/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'volume_mute_outline': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M18.667 11.773v8.453l-2.893-2.893h-3.773v-2.667h3.773l2.893-2.893zm2.666-6.44L14.666 12H9.333v8h5.333l6.667 6.667V5.334z"/>'
  }
})
