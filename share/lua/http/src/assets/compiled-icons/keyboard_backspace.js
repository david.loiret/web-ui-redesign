/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'keyboard_backspace': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M28 14.688v2.625H9.125l4.75 4.813L12 24.001l-8-8 8-8 1.875 1.875-4.75 4.813H28z"/>'
  }
})
