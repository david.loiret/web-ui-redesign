/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'add': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M25.313 17.313h-8v8h-2.625v-8h-8v-2.625h8v-8h2.625v8h8v2.625z"/>'
  }
})
