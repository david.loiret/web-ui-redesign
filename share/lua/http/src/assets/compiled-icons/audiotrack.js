/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'audiotrack': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M16 4h9.313v4H20v14.688h-.063q-.25 2.25-1.938 3.781t-4 1.531q-2.5 0-4.25-1.75T7.999 22t1.75-4.25 4.25-1.75q.875 0 2 .375V4z"/>'
  }
})
