/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Subtitle': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M22.667 9.2v1.333H9.334V9.2h13.333zm0 4v1.333H9.334V13.2h13.333zm-5.334 4v1.333h-8V17.2h8zM25.4 6.6H6.6v14.867h5.933l3.533 3.467 3.533-3.467h5.933V6.6zm1.267-1.267v17.333H20l-4 4-4-4H5.333V5.333h21.333z"/>'
  }
})
