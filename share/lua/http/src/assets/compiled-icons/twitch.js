/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'twitch': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M3 0L0 5v23h8v4h4l4-4h5l9-9V0H3zm23 17l-5 5h-5l-4 4v-4H6V4h20v13z"/><path pid="1" d="M19 8h3v8h-3V8zM13 8h3v8h-3V8z"/>'
  }
})
