/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'settings_input_antenna': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M16 1.313q6.063 0 10.375 4.313t4.313 10.375H28q0-5-3.5-8.5t-8.5-3.5-8.5 3.5-3.5 8.5H1.312q0-6.063 4.313-10.375T16 1.313zm1.313 17.75v4.375l4.563 4.563-1.875 1.875-4-4-4 4-1.875-1.875 4.563-4.563v-4.375q-2-.813-2-3.063 0-1.375.969-2.344t2.344-.969 2.344.969.969 2.344q0 2.25-2 3.063zM16 6.688q3.875 0 6.594 2.719t2.719 6.594h-2.625q0-2.75-1.969-4.719T16 9.313t-4.719 1.969-1.969 4.719H6.687q0-3.875 2.719-6.594T16 6.688z"/>'
  }
})
