/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'playlist_play': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M18.688 18.688l6.625 4-6.625 4v-8zm-13.375 0h10.688v2.625H5.313v-2.625zM5.313 8h16v2.688h-16V8zm0 5.313h16v2.688h-16v-2.688z"/>'
  }
})
