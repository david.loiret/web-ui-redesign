/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Fullscreen': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M19.067 5.333v1.533h5l-17.2 17.2v-5H5.334v7.6h7.6v-1.533h-5l17.2-17.2v5h1.533v-7.6z"/>'
  }
})
