/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Sort': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M19.027 7.02v1.793H5.334V7.02zm0 5.387v1.8H5.334v-1.8zm-4.667 5.386v1.8H5.333v-1.8zm8.467-10.746v15.767l3.02-2.973.82.827-4.393 4.32-4.407-4.32.8-.827 3.013 2.967V7.048zm-8.494 16.14v1.793h-9v-1.793z"/>'
  }
})
