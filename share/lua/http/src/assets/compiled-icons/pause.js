/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'pause': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M18.688 6.688h5.313v18.625h-5.313V6.688zM8 25.313V6.688h5.313v18.625H8z"/>'
  }
})
