/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'volume_down_outline': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M21.333 10.627V21.36c1.973-.973 3.333-3 3.333-5.36a6 6 0 00-3.333-5.373zM6.667 12v8H12l6.667 6.667V5.334L12 12.001H6.667zM16 11.773v8.453l-2.893-2.893H9.334v-2.667h3.773L16 11.773z"/>'
  }
})
