/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'equalizer': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M21.313 12h5.375v14.688h-5.375V12zm-16 14.688V16h5.375v10.688H5.313zm8 0V5.313h5.375v21.375h-5.375z"/>'
  }
})
