/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Loop-One': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M26.333 16.267c-.133.133-.333.267-.533.4s-.467.267-.667.4-.467.2-.667.333c-.2.067-.467.2-.667.267v.933c.2-.067.4-.133.533-.2.2-.067.4-.133.533-.267.133-.067.333-.2.467-.267s.2-.133.333-.267v7.067h1v-8.4h-.333zM16.467 20.2H7.334l2.333-2.333-.733-.733-3.6 3.6 3.6 3.6.733-.733-2.333-2.333h9.133c3.267 0 5.867-2.667 5.867-5.933h-1.067c0 2.667-2.133 4.867-4.8 4.867zM18.067 8.067L20.4 10.4h-9.133C8 10.4 5.4 13.067 5.4 16.333h1.067c0-2.667 2.133-4.867 4.8-4.867h9.067l-2.267 2.333.733.733 3.533-3.6-3.533-3.6-.733.733z"/>'
  }
})
