/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Computer': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M26.667 21V8.2H5.334V21h9.933v1.4H12.4v1.4h7.133v-1.4h-2.867V21h10zM6.733 19.533V9.6H25.2v9.933H6.733z"/>'
  }
})
