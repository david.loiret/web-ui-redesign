/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'volume_up': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M4 12v8h5.333L16 26.667V5.334l-6.667 6.667H4zm18 4a6 6 0 00-3.333-5.373V21.36A5.965 5.965 0 0022 16zM18.667 4.307v2.747c3.853 1.147 6.667 4.72 6.667 8.947s-2.813 7.8-6.667 8.947v2.747C24.014 26.482 28 21.708 28 16.002s-3.987-10.48-9.333-11.693z"/>'
  }
})
