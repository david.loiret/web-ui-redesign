/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'volume_down': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M24.667 16a6 6 0 00-3.333-5.373V21.36A5.965 5.965 0 0024.667 16zm-18-4v8H12l6.667 6.667V5.334L12 12.001H6.667z"/>'
  }
})
