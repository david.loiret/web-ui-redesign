/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'shuffle': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M19.75 17.875l4.188 4.188 2.75-2.75v7.375h-7.375l2.75-2.75-4.188-4.188zm-.437-12.562h7.375v7.375l-2.75-2.75-16.75 16.75-1.875-1.875 16.75-16.75zm-5.188 6.937l-1.875 1.875-6.938-6.938 1.875-1.875z"/>'
  }
})
