/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Next': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M7.113 8.767L17.333 16l-10.22 7.233zm-1.78-3.434v21.333l15.113-10.667zm19.554 21.334h1.78V5.334h-1.78z"/>'
  }
})
