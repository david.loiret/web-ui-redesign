/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'pause_circle_filled': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M20 21.313V10.688h-2.688v10.625H20zm-5.312 0V10.688H12v10.625h2.688zM16 2.688q5.5 0 9.406 3.906T29.312 16t-3.906 9.406T16 29.312t-9.406-3.906T2.688 16t3.906-9.406T16 2.688z"/>'
  }
})
