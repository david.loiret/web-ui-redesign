/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'sort': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M4 17.313v-2.625h16v2.625H4zM4 8h24v2.688H4V8zm0 16v-2.688h8V24H4z"/>'
  }
})
