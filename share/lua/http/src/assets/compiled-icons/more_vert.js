/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'more_vert': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M16 21.313q1.063 0 1.875.813t.813 1.875-.813 1.875-1.875.813-1.875-.813-.813-1.875.813-1.875T16 21.313zm0-8q1.063 0 1.875.813t.813 1.875-.813 1.875-1.875.813-1.875-.813-.813-1.875.813-1.875T16 13.313zm0-2.625q-1.063 0-1.875-.813T13.312 8t.813-1.875T16 5.312t1.875.813T18.688 8t-.813 1.875-1.875.813z"/>'
  }
})
