/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'repeat': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M22.688 22.688v-5.375h2.625v8h-16v4L4 24l5.313-5.313v4h13.375zM9.313 9.313v5.375H6.688v-8h16v-4l5.313 5.313-5.313 5.313v-4H9.313z"/>'
  }
})
