/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Home': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M8.733 15.4L16 8.267l7.267 7.133v9h-4.333v-7.2h-5.8v7.133H8.801V15.4zM16 6.2L5.333 16.733l1 1 .933-.933v9h7.267v-7.133h2.933V25.8h7.267v-9l.933.933 1-1L15.999 6.2z"/>'
  }
})
