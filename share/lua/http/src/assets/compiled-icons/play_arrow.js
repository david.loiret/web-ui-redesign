/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'play_arrow': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M10.688 6.688l14.625 9.313-14.625 9.313V6.689z"/>'
  }
})
