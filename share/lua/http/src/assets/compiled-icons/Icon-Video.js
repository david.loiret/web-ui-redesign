/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Video': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M8 15.353h1.333v1.293H8v-1.293zM8 12.767h1.333v1.293H8v-1.293zM8 17.94h1.333v1.293H8V17.94zM22.667 15.353H24v1.293h-1.333v-1.293z"/><path pid="1" d="M5.333 8.887v14.227h21.333V8.887zm20 12.933H24v-1.293h-1.333v1.293H9.334v-1.293H8.001v1.293H6.668V10.18h1.333v1.293h1.333V10.18h13.333v1.293H24V10.18h1.333z"/><path pid="2" d="M22.667 17.94H24v1.293h-1.333V17.94zM22.667 12.767H24v1.293h-1.333v-1.293z"/>'
  }
})
