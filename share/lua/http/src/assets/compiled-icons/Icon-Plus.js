/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Plus': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M15.267 5.333v9.933H5.334v1.467h9.933v9.933h1.467v-9.933h9.933v-1.467h-9.933V5.333z"/>'
  }
})
