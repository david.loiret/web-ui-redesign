/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'audiotrack_outline': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M16 4v14.067a5.333 5.333 0 00-2.667-.733C10.386 17.334 8 19.721 8 22.667S10.387 28 13.333 28s5.333-2.387 5.333-5.333V9.334h5.333V4.001h-8zm-2.667 21.333c-1.467 0-2.667-1.2-2.667-2.667s1.2-2.667 2.667-2.667S16 21.199 16 22.666s-1.2 2.667-2.667 2.667z"/>'
  }
})
