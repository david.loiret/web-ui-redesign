/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'grid_view': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M25.313 25.313V20H20v5.313h5.313zm-8-8h10.688v10.688H17.313V17.313zm8-5.313V6.687H20V12h5.313zm-8-8h10.688v10.688H17.313V4zM12 25.313V20H6.687v5.313H12zm-8-8h10.688v10.688H4V17.313zM12 12V6.687H6.687V12H12zM4 4h10.688v10.688H4V4z"/>'
  }
})
