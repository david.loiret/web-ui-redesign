/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'delete_outline': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M20.688 5.313h4.625v2.688H6.688V5.313h4.625L12.688 4h6.625zm-10 6.687v13.313h10.625V12H10.688zM8 25.313v-16h16v16q0 1.063-.813 1.875t-1.875.813H10.687q-1.063 0-1.875-.813t-.813-1.875z"/>'
  }
})
