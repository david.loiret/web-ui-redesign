/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'play_circle_filled': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M13.313 22l8-6-8-6v12zM16 2.688q5.5 0 9.406 3.906T29.312 16t-3.906 9.406T16 29.312t-9.406-3.906T2.688 16t3.906-9.406T16 2.688z"/>'
  }
})
