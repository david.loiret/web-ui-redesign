/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Play-Outline': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M11.267 8.267L22.334 16l-11.067 7.733V8.266zM9.733 5.333v21.333L25 15.999 9.733 5.332z"/>'
  }
})
