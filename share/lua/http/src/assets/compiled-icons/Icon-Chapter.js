/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Chapter': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M7.667 5.333v21.333l1.467-1.133 6.867-5.133 6.867 5.133 1.467 1.133V5.333H7.668zm14.8 17.6L16 18.066l-.533.4-5.933 4.467V7.2h13v15.733z"/>'
  }
})
