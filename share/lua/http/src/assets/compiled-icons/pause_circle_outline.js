/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'pause_circle_outline': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M17.313 21.313V10.688h2.688v10.625h-2.688zM16 26.688q4.375 0 7.531-3.156t3.156-7.531-3.156-7.531T16 5.314 8.469 8.47t-3.156 7.531 3.156 7.531T16 26.688zm0-24q5.5 0 9.406 3.906T29.312 16t-3.906 9.406T16 29.312t-9.406-3.906T2.688 16t3.906-9.406T16 2.688zm-4 18.625V10.688h2.688v10.625H12z"/>'
  }
})
