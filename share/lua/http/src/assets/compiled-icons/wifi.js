/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'wifi': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M6.688 17.313q3.875-3.813 9.344-3.813t9.281 3.813l-2.625 2.688q-1.125-1.125-3.094-1.938T16 17.25t-3.594.813-3.094 1.938zM12 22.688Q13.688 21 16 21t4 1.688l-4 4zM1.313 12q6.125-6.063 14.719-6.063T30.688 12L28 14.688Q23 9.75 16 9.75T4 14.688z"/>'
  }
})
