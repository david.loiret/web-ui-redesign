/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Pip': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M14.067 13.667H23.4c.267 0 .533.2.533.533v7.067c0 .267-.2.533-.533.533h-9.333a.525.525 0 01-.533-.533V14.2c0-.333.2-.533.533-.533z"/><path pid="1" d="M25.133 7.533H6.866c-.867 0-1.533.667-1.533 1.533v13.867c0 .867.667 1.533 1.533 1.533h18.2c.867 0 1.533-.667 1.533-1.533V9.066c.067-.867-.6-1.533-1.467-1.533zm.2 15.467H6.666V9h18.667v14z"/>'
  }
})
