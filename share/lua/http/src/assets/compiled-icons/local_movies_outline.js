/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'local_movies_outline': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M18.667 6.667v18.667h-5.333V6.667h5.333zm8-2.667H24v2.667h-2.667V4H10.666v2.667H7.999V4H5.332v24h2.667v-2.667h2.667V28h10.667v-2.667H24V28h2.667V4zm-5.334 8V9.333H24V12h-2.667zM8 12V9.333h2.667V12H8zm13.333 5.333v-2.667H24v2.667h-2.667zM8 17.333v-2.667h2.667v2.667H8zm13.333 5.334V20H24v2.667h-2.667zM8 22.667V20h2.667v2.667H8z"/>'
  }
})
