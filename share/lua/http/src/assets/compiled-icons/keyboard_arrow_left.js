/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'keyboard_arrow_left': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M20.563 22.125L18.688 24l-8-8 8-8 1.875 1.875L14.438 16z"/>'
  }
})
