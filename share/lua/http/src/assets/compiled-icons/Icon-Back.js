/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Back': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M21.667 6.667l-1.133-1.333-10.2 10.667 10.2 10.667 1.133-1.333-8.867-9.333z"/>'
  }
})
