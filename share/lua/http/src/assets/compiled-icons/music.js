/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'music': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M30 0h2v23c0 2.761-3.134 5-7 5s-7-2.239-7-5 3.134-5 7-5c1.959 0 3.729.575 5 1.501V8l-16 3.556V27c0 2.761-3.134 5-7 5s-7-2.239-7-5 3.134-5 7-5c1.959 0 3.729.575 5 1.501V4l18-4z"/>'
  }
})
