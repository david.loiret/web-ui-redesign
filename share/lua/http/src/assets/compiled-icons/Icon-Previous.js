/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Previous': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M24.867 23.8l-10.2-7.267 10.2-7.2V23.8zm-13.334-7.267L26.666 27.2V5.867L11.533 16.534zm-4.4-10.666h-1.8V27.2h1.8V5.867z"/>'
  }
})
