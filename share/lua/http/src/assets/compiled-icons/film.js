/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'film': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M29 0H3a1 1 0 00-1 1v30a1 1 0 001 1h26a1 1 0 001-1V1a1 1 0 00-1-1zM5 28a1 1 0 110-2 1 1 0 010 2zm0-4a1 1 0 110-2 1 1 0 010 2zm0-4a1 1 0 110-2 1 1 0 010 2zm0-4a1 1 0 110-2 1 1 0 010 2zm0-4a1 1 0 110-2 1 1 0 010 2zm0-4a1 1 0 110-2 1 1 0 010 2zm0-4a1 1 0 110-2 1 1 0 010 2zm19 25a1 1 0 01-1 1H9a1 1 0 01-1-1V19a1 1 0 011-1h14a1 1 0 011 1v10zm0-14a1 1 0 01-1 1H9a1 1 0 01-1-1V3a1 1 0 011-1h14a1 1 0 011 1v12zm3 13a1 1 0 110-2 1 1 0 110 2zm0-4a1 1 0 110-2 1 1 0 110 2zm0-4a1 1 0 110-2 1 1 0 110 2zm0-4a1 1 0 110-2 1 1 0 010 2zm0-4a1 1 0 110-2 1 1 0 010 2zm0-4a1 1 0 110-2 1 1 0 010 2zm0-4a1 1 0 110-2 1 1 0 010 2z"/>'
  }
})
