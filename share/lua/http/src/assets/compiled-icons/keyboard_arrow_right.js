/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'keyboard_arrow_right': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M11.438 22.125L17.563 16l-6.125-6.125L13.313 8l8 8-8 8z"/>'
  }
})
