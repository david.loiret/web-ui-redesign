/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Download': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M15.173 8v8.073l-2.087-2.033-1.153 1.12L16 19.12l4.06-3.96-1.153-1.12-2.087 2.033V8zm3.28 0v1.607h6.56v12.78h-18V9.607h6.56V8h-8.24v16h21.333V8z"/>'
  }
})
