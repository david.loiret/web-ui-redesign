/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import { svgIcon } from '../../components/svg-icon/svg-icon.component.js';
svgIcon.register({
  'Icon-Grid': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M5.333 26.667h9.953v-9.953H5.333zm1.42-8.534h7.113v7.113H6.753zM16.713 5.333v9.953h9.953V5.333zm8.534 8.534h-7.113V6.754h7.113zM5.333 15.287h9.953V5.334H5.333zm1.42-8.534h7.113v7.113H6.753zM16.713 26.667h9.953v-9.953h-9.953zm1.42-8.534h7.113v7.113h-7.113z"/>'
  }
})
