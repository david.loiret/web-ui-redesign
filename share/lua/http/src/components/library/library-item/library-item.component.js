Vue.component('library-item', {
    template: '#library-item-template',
    computed: {

    },
    methods: {

    },
    created() {
        this.item = {
            name: 'Some name',
            artist: 'Some artist'
        }
    }
});
