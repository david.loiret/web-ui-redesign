export default MusicViewComponent = {
    template: '#music-view-template',
    methods: { },
    created() {
        document.body.style['overflow-y'] = 'auto';
    }
};
