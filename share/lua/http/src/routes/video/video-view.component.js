export default VideoViewComponent = {
    template: '#video-view-template',
    methods: { },
    created() {
        document.body.style['overflow-y'] = 'auto';
    }
};
