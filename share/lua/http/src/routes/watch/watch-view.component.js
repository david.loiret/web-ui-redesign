export default WatchViewComponent = {
    template: '#watch-view-template',
    methods: { },
    created() {
        document.body.style['overflow-y'] = 'hidden';
    }
};
